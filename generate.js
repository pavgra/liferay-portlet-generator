const path = require('path');
const fs = require('fs');

// Helpers

/**
 * Capitalizes first letter of string.
 *
 * @param {String} string
 * @returns {String}
 */
function ucfirst(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}

/**
 * "Lowercases" first letter of string.
 *
 * @param {String} string
 * @returns {String}
 */
function lcfirst(string) {
  return string.charAt(0).toLowerCase() + string.slice(1);
}

/**
 * Transforms string with spaces to camelCase.
 *
 * @param {String} string
 * @param {boolean} firstLower Flag: whether first char should be in lower case or not.
 */
function toCamelCase(string, firstLower = true) {
  return string
    .split(' ')
    .map((part, i) => {
      if (i === 0 && firstLower) {
        return lcfirst(part);
      }
      return ucfirst(part);
    })
    .join('');
}

/**
 * Checks whether directory exists.
 *
 * @param {any} dirPath
 * @returns
 */
function directoryExists(dirPath) {
  try {
    return fs.statSync(dirPath).isDirectory();
  } catch (err) {
    return false;
  }
}

/**
 * Checks whether path exists and creates folders if smth missed.
 *
 * @param {String} filePath
 * @returns
 */
function ensureDirectoryExistence(filePath) {
  const dirname = path.dirname(filePath);
  if (directoryExists(dirname)) {
    return;
  }
  ensureDirectoryExistence(dirname);
  fs.mkdirSync(dirname);
}

// Builder class

class PortletBuilder {
  /**
   * Creates an instance of PortletBuilder.
   *
   * @param {String} name
   */
  constructor(name) {
    if (!name) {
      throw new Error('Error: Pass name for portlet via first argument!');
    }

    this.name = name;
    this.nameCamel = toCamelCase(name, true);
    this.nameCamelCap = toCamelCase(name, false);
  }

  /**
   * Proceeds indentation of string.
   *
   * @param {String} str
   * @param {number} [spaces=2]
   * @returns
   */
  indent(str, spaces = 2) {
    const initIndentSize = str.split(/\r?\n/).pop().match(/^ */)[0].length;
    const initIndentRegex = new RegExp(`^ {${initIndentSize}}`, 'gm');
    return str
      // remove initial indentation
      .replace(initIndentRegex, '')
      // add required indentation
      .replace(/^/gm, ' '.repeat(spaces));
  }

  /**
   * Generates content of java Controller class.
   *
   * @returns {String}
   */
  getJavaCtrlCode() {
    return this.indent(
      `package com.netcracker.solutions.tfnecu.portal.portlets.${this.nameCamel};
      
      import com.netcracker.portal.framework.JSClass;
      import com.netcracker.portal.framework.portlet.AbstractNcPortletController;
      import org.springframework.stereotype.Controller;
      import org.springframework.ui.Model;
      import org.springframework.web.bind.annotation.RequestMapping;
      
      import javax.portlet.PortletException;
      import javax.portlet.PortletRequest;
      import javax.portlet.PortletResponse;      
      
      @Controller
      @RequestMapping
      @JSClass("nc.portlets.${this.nameCamelCap}Controller")
      public class ${this.nameCamelCap}Controller extends AbstractNcPortletController {      
          @Override
          public void initializePortletData(PortletRequest portletRequest, PortletResponse portletResponse, Model model) throws PortletException {}
      }`, 0);
  }

  /**
   * Generates content of javascript Controller class.
   *
   * @returns {String}
   */
  getJsControllerCode() {
    return this.indent(
      `(function ($) {
        nc.portlets.${this.nameCamelCap}Controller = nc.inheritance.inherits(nc.portlets.NCPortlet, {
            init: function () {
            },

            initDOM: function () {
            }
          }
        );
      })(jQuery);`, 0);
  }

  /**
   * Generates content of Google Closure Template.
   *
   * @returns {String}
   */
  getTemplateCode() {
    return this.indent(
      `{namespace com.netcracker.portal.portlets.${this.nameCamel}.${this.nameCamel}}

      /**
       * Description goes here.
       */
      {template .view}
        <h3>Hello new portlet!</h3>
      {/template}`, 0);
  }

  /**
   * Generates content of java context XML file.
   *
   * @returns {String}
   */
  getContextCode() {
    return this.indent(
      `<?xml version="1.0" encoding="UTF-8"?>
        <beans xmlns="http://www.springframework.org/schema/beans"
          xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
          xmlns:context="http://www.springframework.org/schema/context"
          xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd http://www.springframework.org/schema/context http://www.springframework.org/schema/context/spring-context.xsd">

          <import resource="../portal-portlets-context.xml"/>

          <context:component-scan base-package="com.netcracker.solutions.tfnecu.portal.portlets.${this.nameCamel}"/>
        </beans>`, 0);
  }

  /**
   * Generates content of XML node for liferay-portlet.xml.
   *
   * @returns {String}
   */
  getLiferayPortletXMLNode() {
    return this.indent(
      `<portlet>
        <portlet-name>${this.nameCamel}</portlet-name>
        <!--icon>/icon.png</icon-->
        <instanceable>true</instanceable>
        <requires-namespaced-parameters>false</requires-namespaced-parameters>
        <add-default-resource>true</add-default-resource>
      </portlet>`, 4);
  }

  /**
   * Generates content of XML node for liferay-display.xml.
   *
   * @returns {String}
   */
  getLiferayDisplayXMLNode() {
    return this.indent(`<portlet id="${this.nameCamel}"/>`, 8);
  }

  /**
   * Generates content of XML node for portlet.xml.
   *
   * @returns {String}
   */
  getPortletXMLNode() {
    return this.indent(
      `<portlet>
        <portlet-name>${this.nameCamel}</portlet-name>
        <display-name>${this.name} Portlet</display-name>
        <portlet-class>org.springframework.web.portlet.DispatcherPortlet</portlet-class>
        <init-param>
          <name>contextConfigLocation</name>
          <value>/WEB-INF/portletsCtx/${this.nameCamel}-portlet.xml</value>
        </init-param>
        <expiration-cache>0</expiration-cache>
        <supports>
          <mime-type>text/html</mime-type>
          <portlet-mode>view</portlet-mode>
          <portlet-mode>edit</portlet-mode>
        </supports>
        <portlet-info>
          <title>${this.name} Portlet</title>
          <short-title>${this.name}</short-title>
        </portlet-info>
        <portlet-preferences>
          <preference>
            <name>Template Path</name>
            <value>com.netcracker.portal.portlets.${this.nameCamel}.${this.nameCamel}.view</value>
          </preference>
          <preference>
            <name>JS Path</name>
            <value>js/portlets/${this.nameCamel}/${this.nameCamel}.js</value>
          </preference>
        </portlet-preferences>
      </portlet>`, 4);
  }

  /**
   * Helper for file creation.
   *
   * @param {Object} file
   */
  createFile(file) {
    ensureDirectoryExistence(file.name);
    fs.writeFileSync(file.name, file.content);
    console.log(`Created file: ${file.name}`);
  }

  /**
   * Helper for addition of XML node to end of specified tag.
   *
   * @param {string} tag
   */
  addXMLNode(filePath, tag, content) {
    const xml = fs.readFileSync(filePath, 'utf8');
    fs.writeFileSync(
      filePath,
      xml.replace(`<\/${tag}>`, `${content}\n</${tag}>`)
    );
    console.log(`Added xml node to file: ${filePath}`);
  }

  /**
   * Main function.
   */
  run() {
    // Create Java controller
    this.createFile({
      name: `tfnecu.portal-portlets/src/main/java/com/netcracker/solutions/tfnecu/portal/portlets/${this.nameCamel}/${this.nameCamelCap}Controller.java`,
      content: this.getJavaCtrlCode(),
    });

    // Create JS controller
    this.createFile({
      name: `tfnecu.portal-portlets/src/main/webapp/js/portlets/${this.nameCamel}/${this.nameCamel}.js`,
      content: this.getJsControllerCode(),
    });

    // Create Template
    this.createFile({
      name: `tfnecu.portal-static/src/main/webapp/templates/com/netcracker/portal/portlets/${this.nameCamel}/${this.nameCamel}.soy`,
      content: this.getTemplateCode(),
    });

    // Create Context XML file
    this.createFile({
      name: `tfnecu.portal-portlets/src/main/webapp/WEB-INF/portletsCtx/${this.nameCamel}-portlet.xml`,
      content: this.getContextCode(),
    });

    // Adds node to liferay-portlet.xml.
    this.addXMLNode(
      'tfnecu.portal-portlets/src/main/webapp/WEB-INF/liferay-portlet.xml',
      'liferay-portlet-app',
      this.getLiferayPortletXMLNode()
    );

    // Adds node to liferay-display.xml.
    this.addXMLNode(
      'tfnecu.portal-portlets/src/main/webapp/WEB-INF/liferay-display.xml',
      'category',
      this.getLiferayDisplayXMLNode()
    );

    // Adds node to portlet.xml.
    this.addXMLNode(
      'tfnecu.portal-portlets/src/main/webapp/WEB-INF/portlet.xml',
      'portlet-app',
      this.getPortletXMLNode()
    );
  }
}

// Main code

(function main() {
  // Normalizing command line arguments (first is path to node, second - path to script)
  const args = process.argv.slice(2);

  const portletName = typeof args[0] !== undefined ? args[0] : null;
  const portletPath = typeof args[1] !== undefined ? args[1] : null;

  const portletBuilder = new PortletBuilder(portletName, portletPath);

  portletBuilder.run();
})();
